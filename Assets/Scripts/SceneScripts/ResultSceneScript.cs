﻿using Assets;
using Assets.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.Linq;



public class ResultSceneScript : MonoBehaviour {

    public InputField FildsPersonResults;
	// Use this for initialization
	void Start () {
        List<string> liststring = new List<string>();
        var list= XMLserilization.XmlDeserilization();
        liststring.Add(string.Format("{0} | {1} | {2} | {3} | {4}","Player Name", "Coins", "Time", "Date play", "Why dead"));
        var DescendingList = list.OrderByDescending(x => x.DatePlay).ToList();
        DescendingList.ForEach(x => liststring.Add(string.Format("{0,-20} | {1,8} | {2,8} | {3} | {4}", x.PlayerName, x.CountCoins,x.TimeGame,x.DatePlay.Date.ToShortDateString(),x.whyDead)));
        FildsPersonResults.text=string.Join("\n", liststring.ToArray());
    }

    public void BackToMenuScene()
    {
        SceneManager.LoadScene("HelloScene");
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
