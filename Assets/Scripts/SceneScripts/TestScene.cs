﻿using Assets.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class TestScene : MonoBehaviour {
 
    public void BackToMenuScene()
    {
        SceneManager.LoadScene("HelloScene");
    }
 
}
