﻿using Assets;
using Assets.Models;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Linq;
using System.Xml.Serialization;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
//using UnityEngine.UI;

public class MainSceneScript : MonoBehaviour {


    public InputField InputField;
    public Button DisplayButton;
    public Button ConfirmButton;

    public void ChangeScene (string sceneName) {
        SceneManager.LoadScene(sceneName); 
    }
    void Start()
    {
        DisplayButton.gameObject.SetActive(true);
        ConfirmButton.gameObject.SetActive(false);
        InputField.gameObject.SetActive(false);

        List<Person> list = XMLserilization.XmlDeserilization();

        StaticPlayerInfo.PlayerName =list.Count!=0? list.LastOrDefault().PlayerName:"NewPlayer";
        StaticPlayerInfo.Coins = list.Count != 0 &&list.LastOrDefault().whyDead!= "Death from Mummy enemy" ? list.LastOrDefault().CountCoins:0;

        DisplayButton.GetComponentInChildren<Text>().text = StaticPlayerInfo.PlayerName;

    }
    public void ExitApplication()
    {
        Application.Quit();
    }
   public void OnChangeUserNameClick()
    {
        DisplayButton.gameObject.SetActive(false);
        ConfirmButton.gameObject.SetActive(true);
        InputField.gameObject.SetActive(true);
    }
    public void ConfirmChangeUserNameClick()
    {       
        StaticPlayerInfo.PlayerName = InputField.text.ToString();
        DisplayButton.GetComponentInChildren<Text>().text = StaticPlayerInfo.PlayerName;
        DisplayButton.gameObject.SetActive(true);
        ConfirmButton.gameObject.SetActive(false);
        InputField.gameObject.SetActive(false);
    }
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            this.ExitApplication();
        }
    }
	

}
