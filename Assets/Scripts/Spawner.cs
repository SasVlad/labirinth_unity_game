﻿using Assets.Scripts;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using UnityEngine;
using UnityEngine.UI;

public class Spawner : MonoBehaviour {

    public GameObject[] groups;
    int index_i;
    int index_j;
    public static int lengthLine_X = 20;
    public static int lengthLine_Y = 18;
    private int OccupiedStateGround = 1;
    private int FreeStateGround = 0;
    public enum groupObjects:int
    {
        Wall,
        Coin,
        labirinthPrefab,
        Mummy,
        Player,
        Zombie
    }
    public Block[,] arrayBlock = new Block[lengthLine_X, lengthLine_Y];
    
    public List<GameObject> Coins = new List<GameObject>();
    //в этом методе перебирются все элементы прифаба labirinth с тегом "Ground"
    //координаты каждого эллемента земли заносится в двумерный массив, 
    //в ячейке которого хранятся Point - координаты местонахождени и State - состояние ячейки,
    //0- земля свободная и по ней можно передвигаться, 1 - занятая
    //после по этим координатам будут создаваться и передвигаться элементы игровой сцены
    public void spawnGround()
    {
        var list = groups[(int)groupObjects.labirinthPrefab].GetComponentsInChildren<Transform>().Where(x => x.tag.Equals("Ground")).ToList();
        int i=0,j = 0;
        
        foreach (Transform block in list)
        {      
                if (j< lengthLine_Y)
                {
                    arrayBlock[i, j] = new Block { Point = block.transform.position, StateFree = FreeStateGround };             
                
                }
                else
                {
                    i += 1;
                    j = 0;
                    arrayBlock[i, j] = new Block { Point = block.transform.position, StateFree = FreeStateGround };                   
                }
            j += 1;
        }
    }
    // получение  позиции массива со статусом - свободный
    public void GetRandomXandY(out int out_i, out int out_j)
    {
        do
        {
            out_i = Random.Range(0, lengthLine_X-1);
            out_j = Random.Range(0, lengthLine_Y-1);
        }
        while (arrayBlock[out_i, out_j].StateFree == OccupiedStateGround);
    }

    public void SpawnElement(int i)
    {
        
        GetRandomXandY(out index_i,out index_j);
        Instantiate(groups[i],
                    arrayBlock[index_i, index_j].Point,
                    Quaternion.identity);
        arrayBlock[index_i, index_j].StateFree = OccupiedStateGround;      
    }

    public void spawnInsideWallBlock()
    {
        for (int i = 0; i < 50; i++)
        {
            SpawnElement((int)groupObjects.Wall);
        }
                 
        SpawnElement((int)groupObjects.Player);
        SpawnElement((int)groupObjects.Zombie);
    }

    // Use this for initialization
    void Start () {

        spawnGround();
        spawnInsideWallBlock();
        StartCoroutine(SpawnCoin(5f));
    }

    IEnumerator SpawnCoin(float second)
    {      
        while (true)
        {
            yield return new WaitForSeconds(second);
            if (Coins.Count < 10)
            {
                GetRandomXandY(out index_i, out index_j);

                Coins.Add(Instantiate(groups[(int)groupObjects.Coin],
                    arrayBlock[index_i, index_j].Point,
                    Quaternion.identity));
                arrayBlock[index_i, index_j].StateFree = OccupiedStateGround;
            }
            
        }                         
    }
	// Update is called once per frame
	void Update () {
                                
    }
}
