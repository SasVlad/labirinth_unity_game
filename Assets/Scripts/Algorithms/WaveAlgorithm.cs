﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.Algoritms
{
    public class WaveAlgorithm: MonoBehaviour
    {
        private Vector3 currentPosition;
        private Vector3 lastPosition;
        private bool ready = true;
        private GameObject closestPlayer; //ближайший враг
        private GameObject[] gos; // массив всех врагов
        private float waitMove; // будем перемещать юнитов с задержкой               
        private Spawner spawner;
        int temp_x,temp_y;
        private int lastCountCoins;
        private Text currentCountCoins;
        private int lengthLine_X = 20;
        private int lengthLine_Y = 18;
        private int countCoinsToDownWaitingMove = 20;
        void Start()
        {
            currentPosition = transform.localPosition; // сохраняем текущую позицию
            lastPosition = currentPosition; // сохраняем последную позицию юнита.
                                            // определяем с какой задержкой будет двигаться юнит
            waitMove = UnityEngine.Random.Range(1.4f, 1.65f);
            spawner = GameObject.FindGameObjectWithTag("Spawner").GetComponent<Spawner>();
            currentCountCoins = GameObject.Find("TextCoinsCount").GetComponent<Text>();
            lastCountCoins = int.Parse(currentCountCoins.text);
        }
        //нахождение случайной свободной позиции на карте
        public Vector3 GetRandomXandY()
        {
            do
            {
                temp_x = UnityEngine.Random.Range(0, lengthLine_X-1);
                temp_y = UnityEngine.Random.Range(0, lengthLine_Y-1);
            }
            while (spawner.arrayBlock[temp_x, temp_y].StateFree == 1);
            return spawner.arrayBlock[temp_x, temp_y].Point;
        }

        //возвращаем индекс который соответствует указанным координатам
        private void GetMasIndexByPoints(Block[,] mas,out int out_x,out int out_y,float positionX, float positionY)
        {
            out_x = 0;
            out_y = 0;
            for (int x = 0; x < lengthLine_X; x++)
            {
                for (int y = 0; y < lengthLine_Y; y++)
                {
                    if (mas[x, y].Point.x == positionX && mas[x, y].Point.y == positionY)
                    {
                        out_x = x;
                        out_y = y;
                    }
                }
            }
        }

        void Update()
        {
             if (ready)
             {
                float targetPlayerX, targetPlayerY;
                //если количество монет меньше 20 то враги двигаются случайным образом,
                //при 20 и более монет включается режим слежения за игроком с уменьшением времени ожидания противника
                //с каждой взятой монетой
                if (int.Parse(currentCountCoins.text)< countCoinsToDownWaitingMove)
                {
                    var randomPoint = GetRandomXandY();
                    targetPlayerX = randomPoint.x;
                    targetPlayerY = randomPoint.y;                   
                }
                else
                {
                    //взяли ли пользователь монету
                    if (int.Parse(currentCountCoins.text)> lastCountCoins)
                    {
                        //уменьшаем время ожидания
                        waitMove -= (waitMove * 5 / 100);
                        lastCountCoins = int.Parse(currentCountCoins.text);
                    }
                    //ищем всех объект с тегом Player
                    gos = GameObject.FindGameObjectsWithTag("Player");

                    //находим ближайшего игорока и его координаты
                    GameObject goClosestPlayer = findClosestEnemy();

                    targetPlayerX = goClosestPlayer.transform.localPosition.x;
                    targetPlayerY = goClosestPlayer.transform.localPosition.y;                  
                }

                    Block[,] cMap = findWave(targetPlayerX, targetPlayerY); // находим путь до ближайшего игрока                   

                    // двигаемся, если цель не на соседней клетке
                    // вызываем каротину для перемещения с задержкой
                StartCoroutine(move(cMap, targetPlayerX, targetPlayerY));
                     // запоминаем новую позицию после перемещения и делаем ее текущей
                currentPosition = transform.localPosition;
                // получаем индекс в массиве, который соответствует координатам текущего расположения зомби или мумии
                GetMasIndexByPoints(spawner.arrayBlock,out temp_x,out temp_y, currentPosition.x, currentPosition.y);
                                
                int last_x = 0, last_y = 0;
                // получаем индекс в массиве, который соответствует координатам предыдущему расположению зомби или мумии
                GetMasIndexByPoints(spawner.arrayBlock, out last_x, out last_y, lastPosition.x, lastPosition.y);
               
                //помечаем, что клетка занята противником(мумией или зомби)
                spawner.arrayBlock[temp_x, temp_y].StateFree = 1;                

                //если мы переместились, то на старой клетки пишем, что она освободилась
                if (currentPosition != lastPosition)
                    {
                    spawner.arrayBlock[last_x, last_y].StateFree = 0;
                    lastPosition = currentPosition; // запоминаем текущее рассположение как последнее
                    }
                }
            //}
        }


        /// <summary>РЕАЛИЗАЦИЯ ВОЛНОВОГО АЛГОРИТМА
        ///	</summary>
        /// <param name="cMap">Копия карты локации</param>
        /// <param name="targetX">координата цели x</param>
        /// <param name="targetY">координата цели y</param>
        private IEnumerator move(Block[,] cMap, float targetX, float targetY)
        {
            ready = false;
            float[] neighbors = new float[9];
            int n = 0;
            float curDistance;
            // будем хранить в векторе координаты клетки в которую нужно переместиться
            Vector3 moveTO = new Vector3(-1, 0, 0);

            // получаем индекс в массиве, который соответствует координатам предыдущему расположению игрока
            GetMasIndexByPoints(cMap, out temp_x, out temp_y, currentPosition.x, currentPosition.y);
           
            //первый элемент массива будет наименьшая дистанция клетки куда нужно двигаться
            
            for (int x = temp_x - 1; x <= temp_x + 1; x++)
            {
                for (int y = temp_y + 1; y >= temp_y - 1; y--)
                {
                    // проверяем чтобы не было выхода за массив
                    if (x > 0 && x <= lengthLine_X-1 && y > 0 && y <= lengthLine_Y-1)
                    {
                        curDistance = Vector3.Distance(new Vector3(cMap[x, y].Point.x, cMap[x, y].Point.y, 0), new Vector3(targetX, targetY));
                        //если это стена, помечаем ее как 9999, чтобы она была вконце отсортированного массива
                        if (cMap[x, y].StateFree == -2)
                        {
                            neighbors[n] = 9999;
                        }
                        else
                        {
                            neighbors[n] = curDistance;
                        }
                        n++;
                    }
                    else
                    {
                        neighbors[n] = 9999;
                        n++;
                    }
                }
            }
            Array.Sort(neighbors);

            //----------------------------------------------------------------
            GetMasIndexByPoints(cMap, out temp_x, out temp_y, currentPosition.x, currentPosition.y);

            //ищем координаты клетки с минимальной дистанцией. 
            for (int x = temp_x - 1; x <= temp_x + 1; x++)
            {              
                for (int y = temp_y + 1; y >= temp_y - 1; y--)
                {                    
                    if (x > 0 && x <= lengthLine_X-1 && y > 0 && y <= lengthLine_Y-1)
                    {
                        curDistance = Vector3.Distance(new Vector3(cMap[x, y].Point.x, cMap[x, y].Point.y, 0),new Vector3(targetX, targetY));
                        if (curDistance == neighbors[0])
                        {
                            // и указываем вектору координаты клетки, в которую переместим нашего юнита
                            moveTO = new Vector3(cMap[x, y].Point.x, cMap[x, y].Point.y, 0);
                        }
                    }
                }
            }

            //передвигаемся на новую позицию
            transform.localPosition = moveTO;

            //устанавливаем задержку.
            yield return new WaitForSeconds(waitMove);
            ready = true;
        }

        //Ищмем путь к врагу
        //TargetX, TargetY - координаты ближайшего врага
        private Block[,] findWave(float targetX, float targetY)
        {

            bool add = true; // условие выхода из цикла
                             // делаем копию карты локации, для дальнейшей ее разметки
            Block[,] cMap = new Block[lengthLine_X, lengthLine_Y];
            int x, y, step = 0; // значение шага равно 0
            for (x = 0; x < lengthLine_X; x++)
            {
                for (y = 0; y < lengthLine_Y; y++)
                {                   
                        if (spawner.arrayBlock[x, y].StateFree == 1)
                            cMap[x, y] = new Block { Point = spawner.arrayBlock[x, y].Point, StateFree = -2 }; //если ячейка равна 1, то это стена (пишим -2)
                        else
                        {
                            cMap[x, y] = new Block { Point = spawner.arrayBlock[x, y].Point, StateFree = -1 }; //иначе еще не ступали сюда
                        }
                    //помечаем 0 позицию где распологается игрок
                    if (spawner.arrayBlock[x, y].Point.x == targetX && spawner.arrayBlock[x, y].Point.y == targetY)
                    {
                        cMap[x, y].StateFree = 0;                                              
                    }                   
                }
                
                
            }           
            //начинаем отсчет с финиша, так будет удобней востанавливать путь

            while (add == true)
            {
                add = false;
                for (x = 0; x < lengthLine_X; x++)
                {
                    for (y = 0; y < lengthLine_Y; y++)
                    {
                        if (cMap[x, y].StateFree == step)
                        {
                            // если соседняя клетка не стена, и если она еще не помечена
                            // то помечаем ее значением шага + 1
                            if (y - 1 >= 0 && cMap[x, y - 1].StateFree != -2 && cMap[x, y - 1].StateFree == -1)
                                cMap[x, y - 1].StateFree = step + 1;
                            if (x - 1 >= 0 && cMap[x - 1, y].StateFree != -2 && cMap[x - 1, y].StateFree == -1)
                                cMap[x - 1, y].StateFree = step + 1;
                            if (y + 1 >= 0&& y + 1 < 18 && cMap[x, y + 1].StateFree != -2 && cMap[x, y + 1].StateFree == -1)
                                cMap[x, y + 1].StateFree = step + 1;
                            if (x + 1 >= 0 && x + 1 < 18 && cMap[x + 1, y].StateFree != -2 && cMap[x + 1, y].StateFree == -1)
                                cMap[x + 1, y].StateFree = step + 1;
                        }
                        if (cMap[x, y].Point.x == transform.localPosition.x && cMap[x, y].Point.y == transform.localPosition.y)
                        {
                            temp_x = x; temp_y = y;
                        }
                    }
                }
                step++;
                add = true;
                if (cMap[temp_x, temp_y].StateFree > 0) //решение найдено
                add = false;
                if (step > lengthLine_X * 18) //решение не найдено, если шагов больше чем клеток
                add = false;
            }

            
            return cMap; // возвращаем помеченную матрицу, для востановления пути в методе move()
        }

        // если в сосденей клетки есть игрок, то останавливаемся
        private bool stopMove(Block[,] cMap,float targetX, float targetY)
        {
            bool move = false;
            GetMasIndexByPoints(cMap, out temp_x, out temp_y, currentPosition.x, currentPosition.y);

            int temp_target_x=0, temp_target_y=0;
            GetMasIndexByPoints(cMap, out temp_target_x, out temp_target_y, currentPosition.x, currentPosition.y);

            for (int x = temp_x - 1; x <= temp_x + 1; x++)
            {
                for (int y = temp_y + 1; y >= temp_y - 1; y--)
                {
                    if (x == temp_target_x && y == temp_target_y)
                    {
                        move = true;
                    }
                }
            }
            return move;
        }

        // ищмем ближайшего игрока
        GameObject findClosestEnemy()
        {
            float distance = Mathf.Infinity;
            Vector3 position = transform.position;
            foreach (GameObject go in gos)
            {
                float curDistance = Vector3.Distance(go.transform.position, position);
                if (curDistance < distance)
                {
                    closestPlayer = go;
                    distance = curDistance;
                }
            }
            return closestPlayer;
        }
    }
}
