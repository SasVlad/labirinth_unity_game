﻿using Assets.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using System.Xml.Serialization;

namespace Assets
{
    public static class XMLserilization
    {
        public static void XmlSerilization(Person player)
        {
          
            if (File.Exists("PersonsData.xml"))
            {
                XDocument doc = XDocument.Load("PersonsData.xml");
                XElement root = new XElement("Person");
                root.Add(new XElement("PlayerName", player.PlayerName));
                root.Add(new XElement("CountCoins", player.CountCoins));
                root.Add(new XElement("TimeGame", player.TimeGame));
                root.Add(new XElement("DatePlay", player.DatePlay));
                root.Add(new XElement("whyDead", player.whyDead));
                doc.Element("ArrayOfPerson").Add(root);
                doc.Save("PersonsData.xml");
            }
            else
            {
                XmlSerializer formatter = new XmlSerializer(typeof(List<Person>));
                using (FileStream fs = new FileStream("PersonsData.xml", FileMode.Create))
                {
                    formatter.Serialize(fs, new List<Person>() { player });

                }
            }
        }
        public static List<Person> XmlDeserilization()
        {
            if (File.Exists("PersonsData.xml"))
            {
                List<Person> newListPerson;
                XmlSerializer formatter = new XmlSerializer(typeof(List<Person>));

                using (FileStream fs = new FileStream("PersonsData.xml", FileMode.Open))
                {
                    newListPerson = (List<Person>)formatter.Deserialize(fs);
                }
                return newListPerson;


            }
            return new List<Person>();
        }

    }
}
