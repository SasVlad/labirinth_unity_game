﻿using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;

public class AIController : MonoBehaviour {

    public Animator animator;
    Rigidbody2D rigBody;

    void Start () {
        animator= GetComponentInChildren<Animator>();
        rigBody = GetComponent<Rigidbody2D>();
    }
    
    void OnTriggerStay2D(Collider2D col)
    {
        if (col.tag=="Player")
        {
            animator.SetBool("IsAttacking", true);
        }
    }
    void OnTriggerExit2D(Collider2D col)
    {
        if (col.tag == "Player")
        {
            animator.SetBool("IsAttacking", false);
        }
    }
    
}
