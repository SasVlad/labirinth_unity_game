﻿using Assets;
using Assets.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class playerController : MonoBehaviour {

    private float timer=0;
    private int index = 0;
    private Text CountCoins;
    float velX;
    float velY;
    bool facingRight = true;
    Rigidbody2D rigBody;
    private Person player;
    private Spawner spawner;
    private int lengthLine_X = 20;
    private int lengthLine_Y = 18;
    private int mummyIndex = 3;
    private int zombieIndex = 5;
    // Use this for initialization
    void Start()
    {
        //создаем объект с первичными данными: Именем, датой и количеством монет
        player = new Person()
        {
            PlayerName = StaticPlayerInfo.PlayerName,
            CountCoins = StaticPlayerInfo.Coins,
            DatePlay = DateTime.Now
        };
        spawner = GameObject.FindGameObjectWithTag("Spawner").GetComponent<Spawner>();
        CountCoins = GameObject.Find("TextCoinsCount").GetComponent<Text>();
        index = player.CountCoins;
        GameObject.Find("TextPlayerName").GetComponent<Text>().text = player.PlayerName;
        CountCoins.text = player.CountCoins.ToString();      
        rigBody = GetComponent<Rigidbody2D>();
        SpawnEnemyByCoin();
    }
    //создание противников по количеству монет
    void SpawnEnemyByCoin()
    {
        //если у пользователя больше 5 монет, создаем еще одного зомби
        if (index >= 5 && GameObject.FindGameObjectsWithTag("ZombieEnemy").Length<2)
        {
            spawner.SpawnElement(zombieIndex);
        }
        //если у пользователя больше 10 монет, создаем муммию
        if (index >= 10&&GameObject.FindGameObjectsWithTag("MummyEnemy").Length < 1)
        {
            spawner.SpawnElement(mummyIndex);
        }
    }

    //получения индекса массива по кординатам
    private int GetMasIndexByPoints(float positionX, float positionY)
    {
        int out_state = 0;
        for (int x = 0; x < lengthLine_X; x++)
        {
            for (int y = 0; y < lengthLine_Y; y++)
            {
                if (spawner.arrayBlock[x, y].Point.x == positionX && spawner.arrayBlock[x, y].Point.y == positionY)
                {
                    out_state = spawner.arrayBlock[x, y].StateFree;
                }
            }
        }
        return out_state;
    }

    void Update()
    {

        timer += Time.deltaTime;
        GameObject.Find("TimerText").GetComponent<Text>().text = ((int)timer).ToString();
        //при нажатии Esc выходим из игры с сохранением разультатов
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            this.SaveResultWhenDeadOrExit("Exit from game");
            Application.Quit();
        }
        //передвижение игрока по клеткам при нажатии на стрелки или (w,s,a,d)
        float differ_y = spawner.arrayBlock[0, 1].Point.y - spawner.arrayBlock[0, 0].Point.y;
        float differ_x = spawner.arrayBlock[0, 0].Point.x - spawner.arrayBlock[1, 0].Point.x;

        if (Input.GetKeyDown(KeyCode.UpArrow) || Input.GetKeyDown(KeyCode.W))
        {
            differ_x = transform.localPosition.x;
            differ_y = transform.localPosition.y + differ_y;
        }
        else if (Input.GetKeyDown(KeyCode.DownArrow) || Input.GetKeyDown(KeyCode.S))
        {
            differ_x = transform.localPosition.x;
            differ_y = transform.localPosition.y - differ_y;
        }
        else if (Input.GetKeyDown(KeyCode.LeftArrow) || Input.GetKeyDown(KeyCode.A))
        {
            differ_x = transform.localPosition.x + differ_x;
            differ_y = transform.localPosition.y;          
        }
        else if (Input.GetKeyDown(KeyCode.RightArrow) || Input.GetKeyDown(KeyCode.D))
        {
            differ_x = transform.localPosition.x - differ_x;
            differ_y = transform.localPosition.y;
        }
        else return;
       
            rigBody.MovePosition(new Vector2(differ_x, differ_y));

        //velX = Input.GetAxisRaw("Horizontal");
        //velY = Input.GetAxisRaw("Vertical");
        //rigBody.velocity = new Vector2(velX * moveSpeed, velY * moveSpeed);
    }
    //сериализация результатом при смерти или выходе из игры
    private void SaveResultWhenDeadOrExit(string reasonToDeath)
    {
        player.CountCoins = index;
        player.TimeGame = (int)timer;
        player.whyDead = reasonToDeath;
        XMLserilization.XmlSerilization(player);
    }
    void OnTriggerEnter2D(Collider2D col)
    {
        //если колайдер игрока соприкосается с коллайдером монеты то мы ее берем
        if (col.tag=="Coin")
        {
            spawner.Coins.Remove(col.gameObject);
            ++index;
            CountCoins.text = index.ToString();
            Destroy(col.gameObject);
            SpawnEnemyByCoin();
        }
        
    }
    void OnCollisionEnter2D(Collision2D col)
    {
        //при столкновением с зомби игра завершается,переходим на сцену HelloScene, результаты сохраняются в файл
        //при возобновлении игры после смерти количество монеты востанавливается
        if (col.collider.tag == "ZombieEnemy")
        {         
            this.SaveResultWhenDeadOrExit("Death from Zombie enemy");
            Destroy(col.gameObject);           
            SceneManager.LoadScene("HelloScene");
        }
        //при столкновением с мумией игра завершается,переходим на сцену HelloScene, результаты сохраняются в файл
        //при возобновлении игры после смерти количество монеты обнуляется
        if (col.collider.tag == "MummyEnemy")
        {
            this.SaveResultWhenDeadOrExit("Death from Mummy enemy");
            Destroy(col.gameObject);
            SceneManager.LoadScene("HelloScene");
        }
    }

}
