﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Models
{
    [Serializable]
    public class Person
    {
        public string PlayerName { get; set; }
        public int CountCoins { get; set; }
        public int TimeGame { get; set; }
        public DateTime DatePlay { get; set; }
        public string whyDead { get; set; }
    }
}
