﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class StaticPlayerInfo{

    public static string PlayerName { get; set; }
    public static int Coins { get; set; }    
}
