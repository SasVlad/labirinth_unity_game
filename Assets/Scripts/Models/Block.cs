﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts
{
    public class Block
    {
        public int StateFree { get; set; }
        public Vector3 Point { get; set; }

    }
}
